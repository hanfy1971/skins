#!/bin/sh
echo ''
echo '************************************************************'
echo "**                         STARTED                        **"
echo '************************************************************'
echo "**                 Uploaded by: TAREK-HANFY                  **"
echo "************************************************************"
echo ''
sleep 3s

if [ -d /usr/share/enigma2/Aglare-FHD ]; then
echo "> removing package please wait..."
sleep 3s 
rm -rf /usr/share/enigma2/Aglare-FHD > /dev/null 2>&1

status='/var/lib/opkg/status'
package='enigma2-plugin-skins-aglare-fhd'

if grep -q $package $status; then
opkg remove $package > /dev/null 2>&1
fi

echo "*******************************************"
echo "*             Removed Finished            *"
echo "*            Uploaded By TAREK_HANFY          *"
echo "*******************************************"
sleep 3s

else

#remove unnecessary files and folders
if [  -d "/CONTROL" ]; then
rm -r  /CONTROL >/dev/null 2>&1
fi
rm -rf /control >/dev/null 2>&1
rm -rf /postinst >/dev/null 2>&1
rm -rf /preinst >/dev/null 2>&1
rm -rf /prerm >/dev/null 2>&1
rm -rf /postrm >/dev/null 2>&1
rm -rf /tmp/*.ipk >/dev/null 2>&1
rm -rf /tmp/*.tar.gz >/dev/null 2>&1

status='/var/lib/opkg/status'
package='enigma2-plugin-skins-aglare-fhd'
package1='enigma2-plugin-skins-aglare-fhd'
if grep -q $package $status; then
opkg install $package > /dev/null 2>&1
fi
if grep -q $package1 $status; then
opkg install $package1 > /dev/null 2>&1
fi

#config
plugin=aglare-fhd
version=2.4
url=https://gitlab.com/hanfy1971/skins/-/raw/main/aglare-fhd-2.4.tar.gz
package=/var/volatile/tmp/$plugin-$version.tar.gz

#download & install
echo "> Downloading $plugin-$version package  please wait ..."
sleep 3s

wget -O $package --no-check-certificate $url
tar -xzf $package -C /
extract=$?
rm -rf $package >/dev/null 2>&1

echo ''
if [ $extract -eq 0 ]; then
SKINDIR='/usr/share/enigma2/Aglare-FHD'
TMPDIR='/tmp'
set -e
if grep -qs -i "openATV" /etc/image-version; then
mv $SKINDIR/image_logo/openatv/imagelogo.png $SKINDIR
   
elif grep -qs -i "egami" /etc/image-version; then
mv $SKINDIR/image_logo/egami/imagelogo.png $SKINDIR
	
elif grep -qs -i "PURE2" /etc/image-version; then
mv $SKINDIR/image_logo/pure2/imagelogo.png $SKINDIR
	
elif grep -qs -i "OpenSPA" /etc/image-version; then
mv $SKINDIR/image_logo/openspa/imagelogo.png $SKINDIR
	
else
echo ""
fi
sleep 2
rm -rf $SKINDIR/image_logo  > /dev/null 2>&1
rm -rf /control  > /dev/null 2>&1
sleep 2
set +e
echo "> $plugin-$version package installed successfully"
echo "> Uploaded By Tarek "
sleep 3s

else

echo "> $plugin-$version package installation failed"
sleep 3s
fi

fi
