#!/bin/bash
echo ''
echo '************************************************************'
echo "**                         STARTED                        **"
echo '************************************************************'
echo "**                 Uploaded by: Tarekha                   **"
echo "************************************************************"
echo ''
sleep 3s

wget -O /tmp/aglare-fhd-pli-3.2.tar.gz "https://gitlab.com/hanfy1971/skins/-/raw/main/pli/aglare-fhd-pli-3.2.tar.gz"
tar -xzf /tmp/*.tar.gz -C /

rm -r /tmp/aglare-fhd-pli-3.2.tar.gz

sleep 2;
echo "############ INSTALLATION COMPLETED ########"
echo "############ RESTARTING... #################" 
init 4
sleep 2
init 3
exit 

