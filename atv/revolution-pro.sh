if [ -d /usr/share/enigma2/skin_revolution_pro ]; then
echo "> removing package please wait..."
sleep 3s 
rm -rf /usr/share/enigma2/skin_revolution_pro >/dev/null 2>&1
rm -rf /usr/lib/enigma2/python/Plugins/Extensions/skin_revolution_pro >/dev/null 2>&1

echo "*******************************************"
echo "*             Removed Finished            *"
echo "*            Uploaded By TAREK-HANFY          *"
echo "*******************************************"
sleep 3s

else

#config
skin=revolution-pro
version=6.3
url=https://gitlab.com/hanfy1971/skins/-/raw/main/atv/revolution-pro-6.3.tar.gz
package=/var/volatile/tmp/$skin-$version.tar.gz

echo "> Downloading $skin-$version skin  please wait ..."
sleep 3s

#remove unnecessary files and folders
if [  -d "/CONTROL" ]; then
rm -r  /CONTROL >/dev/null 2>&1
fi
rm -rf /control >/dev/null 2>&1
rm -rf /postinst >/dev/null 2>&1
rm -rf /preinst >/dev/null 2>&1
rm -rf /prerm >/dev/null 2>&1
rm -rf /postrm >/dev/null 2>&1
rm -rf /tmp/*.ipk >/dev/null 2>&1
rm -rf /tmp/*.tar.gz >/dev/null 2>&1

wget -O $package --no-check-certificate $url
tar -xzf $package -C /
extract=$?
rm -rf package >/dev/null 2>&1

echo ''
if [ $extract -eq 0 ]; then 
echo "> $skin-$version skin installed successfully"
echo "> Uploaded By TAREK"
sleep 3s
else
echo "> $skin-$version skin installation failed"
sleep 3s
fi
fi

exit 0
