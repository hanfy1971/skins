#!/bin/sh

# Configuration
#########################################
plugin="aglare-fhd"
git_url="https://gitlab.com/hanfy1971/skins/-/raw/main/Aglare"
version=$(wget $git_url/version -qO- | awk 'NR==1')
plugin_path="usr/share/enigma2/Aglare-FHD"
package="enigma2-plugin-skins-$plugin"
targz_file="$plugin.tar.gz"
url="$git_url/$targz_file"
temp_dir="/tmp"

# Determine package manager
#########################################
if command -v dpkg &> /dev/null; then
package_manager="apt"
status_file="/var/lib/dpkg/status"
uninstall_command="apt-get purge --auto-remove -y"
else
package_manager="opkg"
status_file="/var/lib/opkg/status"
uninstall_command="opkg remove --force-depends"
fi

#check and_remove package old version
#########################################
check_and_remove_package() {
if [ -d $plugin_path ]; then
echo "> removing package old version please wait..."
sleep 3 
rm -rf $plugin_path > /dev/null 2>&1
rm -rf /usr/lib/enigma2/python/Plugins/Extensions/Aglare  > /dev/null 2>&1
rm -r /usr/lib/enigma2/python/Components/Aglare* > /dev/null 2>&1
rm -r /usr/lib/enigma2/python/Components/Converter/Aglare* > /dev/null 2>&1
rm -r /usr/lib/enigma2/python/Components/Renderer/Aglare* > /dev/null 2>&1

if grep -q "$package" "$status_file"; then
echo "> Removing existing $package package, please wait..."
$uninstall_command $package > /dev/null 2>&1
fi
echo "*******************************************"
echo "*             Removed Finished            *"
echo "*            Uploaded By TAREK          *"
echo "*******************************************"
sleep 3
exit 1
else
echo " " 
fi  }
check_and_remove_package

#check & install dependencies
#########################################
check_and_install_dependencies() {
# Determine package manager
if command -v dpkg &> /dev/null; then
    install_command="apt-get install"
else
    install_command="opkg install"
fi
$install_command enigma2-plugin-extensions-bitrate enigma2-plugin-extensions-oaweather >/dev/null 2>&1
}
check_and_install_dependencies

#download & install package
#########################################
download_and_install_package() {
echo "> Downloading $plugin-$version package  please wait ..."
sleep 3
wget --show-progress -qO $temp_dir/$targz_file --no-check-certificate $url
tar -xzf $temp_dir/$targz_file -C / > /dev/null 2>&1
extract=$?
rm -rf $temp_dir/$targz_file >/dev/null 2>&1

if [ $extract -eq 0 ]; then
SKINDIR='/usr/share/enigma2/Aglare-FHD'
TMPDIR='/tmp'
set -e
if grep -qs -i "openATV" /etc/image-version; then
mv $SKINDIR/image_logo/openatv/imagelogo.png $SKINDIR
rm -rf /usr/share/enigma2/Aglare-FHD/skin_egamicam.xml  > /dev/null 2>&1
rm -rf /usr/share/enigma2/Aglare-FHD/skin_pure2cam.xml  > /dev/null 2>&1 
   
elif grep -qs -i "egami" /etc/image-version; then
mv $SKINDIR/image_logo/egami/imagelogo.png $SKINDIR
rm -rf /usr/share/enigma2/Aglare-FHD/skin_atvcam.xml  > /dev/null 2>&1
rm -rf /usr/share/enigma2/Aglare-FHD/skin_pure2cam.xml  > /dev/null 2>&1
	
elif grep -qs -i "PURE2" /etc/image-version; then
mv $SKINDIR/image_logo/pure2/imagelogo.png $SKINDIR
rm -rf /usr/share/enigma2/Aglare-FHD/skin_egamicam.xml  > /dev/null 2>&1
rm -rf /usr/share/enigma2/Aglare-FHD/skin_atvcam.xml  > /dev/null 2>&1
	
elif grep -qs -i "OpenSPA" /etc/image-version; then
mv $SKINDIR/image_logo/openspa/imagelogo.png $SKINDIR
rm -rf /usr/share/enigma2/Aglare-FHD/skin_egamicam.xml  > /dev/null 2>&1
rm -rf /usr/share/enigma2/Aglare-FHD/skin_atvcam.xml  > /dev/null 2>&1
	
else
echo ""
fi
sleep 2

rm -rf $SKINDIR/image_logo  > /dev/null 2>&1
rm -rf /control  > /dev/null 2>&1
set +e
  echo "> $plugin-$version package installed successfully"
  sleep 3
  echo ""
else
  echo "> $plugin-$version package download failed"
  sleep 3
fi  }
download_and_install_package

# Remove unnecessary files and folders
#########################################
print_message() {
echo "> [$(date +'%Y-%m-%d')] $1"
}
cleanup() {
[ -d "/CONTROL" ] && rm -rf /CONTROL >/dev/null 2>&1
rm -rf /control /postinst /preinst /prerm /postrm /tmp/*.ipk /tmp/*.tar.gz >/dev/null 2>&1
print_message "> Uploaded By TAREK "
}
cleanup
   
