#!/bin/sh
echo ''
echo '************************************************************'
echo "**                         STARTED                        **"
echo '************************************************************'
echo "**                 Uploaded by: TAREK-HANFY                   **"
echo "************************************************************"
echo ''
sleep 3s

if [ -d /usr/share/enigma2/Aglare-FHD-PLI ]; then
echo "> removing package please wait..."
sleep 3s 
rm -rf /usr/share/enigma2/Aglare-FHD-PLI > /dev/null 2>&1

status='/var/lib/opkg/status'
package='enigma2-plugin-skins-aglare-fhd'

if grep -q $package $status; then
opkg remove $package > /dev/null 2>&1
fi

echo "*******************************************"
echo "*             Removed Finished            *"
echo "*            Uploaded By TAREK-HANFY         *"
echo "*******************************************"
sleep 3s

else

#remove unnecessary files and folders
if [  -d "/CONTROL" ]; then
rm -r  /CONTROL >/dev/null 2>&1
fi
rm -rf /control >/dev/null 2>&1
rm -rf /postinst >/dev/null 2>&1
rm -rf /preinst >/dev/null 2>&1
rm -rf /prerm >/dev/null 2>&1
rm -rf /postrm >/dev/null 2>&1
rm -rf /tmp/*.ipk >/dev/null 2>&1
rm -rf /tmp/*.tar.gz >/dev/null 2>&1


#config
plugin=aglare-fhd-pli
version=2.3
url=https://gitlab.com/hanfy1971/skins/-/raw/main/aglare-fhd-pli-2.3.tar.gz
package=/var/volatile/tmp/$plugin-$version.tar.gz

#download & install
echo "> Downloading $plugin-$version package  please wait ..."
sleep 3s

wget -O $package --no-check-certificate $url
tar -xzf $package -C /
extract=$?
SKINDIR='/usr/share/enigma2/Aglare-FHD-PLI/'
TMPDIR='/tmp'
set -e
if grep -qs -i "openbh" /etc/image-version; then
mv $SKINDIR/image_logo/obh/imagelogo.png $SKINDIR
	
elif grep -qs -i "openvix" /etc/image-version; then
mv $SKINDIR/image_logo/openvix/imagelogo.png $SKINDIR
	
elif grep -qs -i "openpli" /etc/issue; then
mv $SKINDIR/image_logo/openpli/imagelogo.png $SKINDIR

elif grep -qs -i "areadeltasat" /etc/issue; then
mv $SKINDIR/image_logo/openpli/imagelogo.png $SKINDIR

elif grep -qs -i "nonsolosat" /etc/issue; then
mv $SKINDIR/image_logo/openpli/imagelogo.png $SKINDIR
	
elif grep -qs -i "satlodge" /etc/issue; then
mv $SKINDIR/image_logo/satlodge/imagelogo.png $SKINDIR
else	
echo ""
fi
rm -rf $SKINDIR/image_logo  > /dev/null 2>&1
rm -rf /control  > /dev/null 2>&1
set +e
sleep 2
rm -rf $package >/dev/null 2>&1

echo ''
if [ $extract -eq 0 ]; then
echo "> $plugin-$version package installed successfully"
echo "> Uploaded By Tarek "
sleep 3s

else

echo "> $plugin-$version package installation failed"
sleep 3s
fi

fi
